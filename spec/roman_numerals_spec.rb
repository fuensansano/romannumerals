require_relative '../src/roman_numerals.rb' 
describe 'RomanNumerals' do
  describe 'about I-III' do
    it 'should return I when given 1' do
      expect(RomanNumerals.convert(1)).to eq('I')
    end

    it 'should return II when given 2' do
      expect(RomanNumerals.convert(2)).to eq('II')
    end

    it 'should return III when given 3' do
      expect(RomanNumerals.convert(3)).to eq('III')
    end
  end

  describe 'about IV' do
    it 'should return IV when given 4' do
      expect(RomanNumerals.convert(4)).to eq('IV')
    end
  end
  
  describe 'about V-VIII' do
    it 'should return V when given 5' do
      expect(RomanNumerals.convert(5)).to eq('V')
    end

    it 'should return VI when given 6' do
      expect(RomanNumerals.convert(6)).to eq('VI')
    end

    it 'should return VIII when given 8' do
      expect(RomanNumerals.convert(8)).to eq('VIII')
    end
  end

  describe 'about IX' do
    it 'should return IX when given 9' do
      expect(RomanNumerals.convert(9)).to eq('IX')
    end
  end

  describe 'about X-XIII' do
    it 'should return X when given 10' do
      expect(RomanNumerals.convert(10)).to eq('X')
    end

    it 'should return XI when given 11' do
      expect(RomanNumerals.convert(11)).to eq('XI')
    end

    it 'should return XIII when given 13' do
      expect(RomanNumerals.convert(13)).to eq('XIII')
    end
  end

  describe 'about XIV' do
    it 'should return XIV when given 14' do
      expect(RomanNumerals.convert(14)).to eq('XIV')
    end
  end
end