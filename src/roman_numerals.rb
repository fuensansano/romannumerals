class RomanNumerals
    def self.convert(decimal_number)
      return 'XIV' if (decimal_number === 14)
      return 'IX' if (decimal_number === 9)
      return 'IV' if (decimal_number === 4)

      if (decimal_number >= 10) then
        roman_number = 'X'
        decimal_number -= 10
        roman_number += RomanNumerals.concatI(decimal_number)
        return roman_number
      end

      if (decimal_number >= 5) then
        roman_number = 'V'
        decimal_number -= 5
        roman_number += RomanNumerals.concatI(decimal_number)
        return roman_number
      end
    
      RomanNumerals.concatI(decimal_number)

    end


  
    def self.concatI(decimal_number)
      roman_numeral = ''
  
      for i in (1..decimal_number) do
        roman_numeral += 'I'
      end
  
      roman_numeral
    end
end